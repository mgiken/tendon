require "../spec_helper"

describe Tendon::Environment do
  describe "#to_s" do
    context "with development environment" do
      it "returns `development`" do
        ENV["TENDON_ENV"] = "development"
        Tendon::Environment.new.to_s.should eq "development"
      end
    end

    context "with productioon environment" do
      it "returns `productioon`" do
        ENV["TENDON_ENV"] = "productioon"
        Tendon::Environment.new.to_s.should eq "productioon"
      end
    end

    context "with foo" do
      it "returns `foo`" do
        Tendon::Environment.new("foo").to_s.should eq "foo"
      end
    end
  end

  describe "#development?" do
    context "with development environment" do
      it "returns true" do
        ENV["TENDON_ENV"] = "development"
        Tendon::Environment.new.development?.should be_true
      end
    end

    context "with productioon environment" do
      it "returns false" do
        ENV["TENDON_ENV"] = "productioon"
        Tendon::Environment.new.development?.should be_false
      end
    end

    context "with foo" do
      it "returns false" do
        Tendon::Environment.new("foo").development?.should be_false
      end
    end
  end

  describe "#productioon?" do
    context "with development environment" do
      it "returns false" do
        ENV["TENDON_ENV"] = "development"
        Tendon::Environment.new.productioon?.should be_false
      end
    end

    context "with productioon environment" do
      it "returns true" do
        ENV["TENDON_ENV"] = "productioon"
        Tendon::Environment.new.productioon?.should be_true
      end
    end

    context "with foo" do
      it "returns false" do
        Tendon::Environment.new("foo").productioon?.should be_false
      end
    end
  end
end
