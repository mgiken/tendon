require "./spec_helper"

describe Tendon do
  it "has a version number" do
    Tendon::VERSION.should_not be_empty
  end

  describe ".env" do
    it "returns a singleton instance of Tendon::Environment class" do
      Tendon.env.should be_a Tendon::Environment
      Tendon.env.should eq Tendon.env
    end
  end

  describe ".env=" do
    it "sets the environment" do
      Tendon.env = "foo"
      Tendon.env.foo?.should be_true
    end
  end

  describe ".config" do
    it "returns a singleton instance of Tendon::Config class" do
      Tendon.config.should be_a Tendon::Config
      Tendon.config.should eq Tendon.config
    end
  end

  describe ".configure" do
    it "sets config" do
      Tendon.configure do |config|
        config.host = "192.168.0.1"
      end
      Tendon.config.host.should eq "192.168.0.1"
    end
  end

  describe ".logger" do
    it "returns a singleton instance of Soma::Logger class" do
      Tendon.logger.should be_a Soma::Logger
      Tendon.logger.should eq Tendon.logger
    end
  end
end
