BUILD_ARGS = $(if $(filter production,$(CRYSTAL_ENV)),--production)
SPEC_ARGS  = $(if $(filter production,$(CRYSTAL_ENV)),--release)

define RUN
	@printf "\033[34m%s\033[m "  "==>"
 	$1
endef

.PHONY: test
test: info
	$(call RUN, shards check || shards install)
	$(call RUN, crystal spec --warnings=all --error-on-warnings $(SPEC_ARGS))
	$(call RUN, crystal tool format --check)
	$(call RUN, bin/ameba --all)

.PHONY: info
info:
	$(call RUN, crystal version)
	$(call RUN, crystal env)
	@echo CRYSTAL_ENV="$(CRYSTAL_ENV)"
