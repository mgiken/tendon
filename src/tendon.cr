require "soma"

require "./ext/**"
require "./tendon/**"

module Tendon
  VERSION = {{ `shards version #{__DIR__}`.stringify.chomp }}

  extend self

  def env
    @@env ||= Environment.new
  end

  def env=(env)
    @@env = Environment.new(env)
  end

  def config
    @@config ||= Config.new
  end

  def configure
    yield config
  end

  def logger
    @@logger ||= Soma::Logger.new(config.log_io, config.log_level, config.log_formatter)
  end

  def run
    s = Tendon::Server.new

    Signal::INT.trap do
      Signal::INT.reset

      Tendon::DB.close
      s.close
      logger.warn("Shutting down Tendon server")
    end

    logger.debug("Starting Tendon server", host: config.host, port: config.port, env: env.to_s, ver: VERSION)
    s.listen
  rescue e
    logger.fatal(e)
  end
end
