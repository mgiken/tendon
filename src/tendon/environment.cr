class Tendon::Environment
  ENV_KEY = "TENDON_ENV"
  DEFAULT = "development"

  def initialize(@env : String = ENV.fetch(ENV_KEY, DEFAULT))
    ENV[ENV_KEY] ||= @env
  end

  def to_s(io)
    io << @env
  end

  macro method_missing(call)
    {% if call.name.ends_with?('?') %}
      @env == {{ call.name.id.stringify[0..-2] }}
    {% else %}
      super
    {% end %}
  end
end
