require "../tendon"

class Tendon::Config
  property host = "0.0.0.0"
  property port = 8080

  property public_directory = "./public"
  property directory_listing = false

  property compress_available_types = %w[.txt .html .xml .css .js .json .svg]

  property log_io : IO
  property log_level : Logger::Severity
  property log_formatter : Soma::Formatter

  property enable_ssl = true

  property jsonrpc_endpoint : String = "/jsonrpc"

  def initialize
    @log_io = STDOUT
    @log_formatter = log_io.tty? ? Soma::Formatter::Terminal.new : Soma::Formatter::JSON.new
    @log_level = Tendon.env.development? ? Logger::DEBUG : Logger::INFO
  end
end
