class Tendon::Server
  delegate listen, close, to: @server
  private delegate config, to: Tendon

  def initialize
    h = [] of ::HTTP::Handler
    h << Tendon::Handler::LogHandler.new
    h << Tendon::Handler::ErrorHandler.new
    h << Tendon::Handler::ProtectHandler.new(config.enable_ssl)
    h << Tendon::Handler::CompressHandler.new(config.compress_available_types)
    h << Tendon::Handler::JSONRPCHandler.new

    if Dir.exists?(config.public_directory)
      h << Tendon::Handler::StaticFileHandler.new(config.public_directory, directory_listing: config.directory_listing)
    end

    @server = ::HTTP::Server.new(h)
    @server.bind_tcp(config.host, config.port)
  end
end
