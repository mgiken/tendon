require "pg"

module Tendon::DB
  @@db : ::DB::Database?

  def self.db
    @@db ||= ::DB.open(ENV["DATABASE_URL"])
  end

  def self.close
    @@db.try(&.close)
  end

  def db
    Tendon::DB.db
  end

  def transaction
    db.retry do
      ret = nil
      db.transaction do |tx|
        ret = yield tx.connection
      end
      ret
    end
  end
end
