require "http"

module Tendon::HTTP
  class Error < Exception
    getter status : ::HTTP::Status

    delegate code, to: status

    def initialize(@status, @cause = nil)
      @message = status.description
    end
  end

  class BadRequest < Error
    def initialize(cause = nil)
      super(::HTTP::Status::BAD_REQUEST, cause)
    end
  end

  class Unauthorized < Error
    def initialize(cause = nil)
      super(::HTTP::Status::UNAUTHORIZED, cause)
    end
  end

  class NotFound < Error
    def initialize(cause = nil)
      super(::HTTP::Status::NOT_FOUND, cause)
    end
  end

  class MethodNotAllowed < Error
    def initialize(cause = nil)
      super(::HTTP::Status::METHOD_NOT_ALLOWED, cause)
    end
  end

  class UnsupportedMediaType < Error
    def initialize(cause = nil)
      super(::HTTP::Status::UNSUPPORTED_MEDIA_TYPE, cause)
    end
  end

  class InternalServerError < Error
    def initialize(cause = nil)
      super(::HTTP::Status::INTERNAL_SERVER_ERROR, cause)
    end
  end
end
