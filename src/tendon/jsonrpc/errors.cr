require "json"

module Tendon::JSONRPC
  abstract class Error < Exception
    getter code, data

    def initialize(@message, @code, @cause = nil, data = nil)
      @data = JSON.parse(data.to_json) if data
    end

    JSON.mapping(message: String?, code: Int32, data: JSON::Any?)
  end

  class ApplicationError < Error
    def initialize(message, cause = nil, data = nil)
      super(message, -32_000, cause, data)
    end
  end

  class InvalidRequest < Error
    def initialize(cause = nil, data = nil)
      super("Invalid request", -32_600, cause, data)
    end
  end

  class MethodNotFound < Error
    def initialize(cause = nil, data = nil)
      super("Method not found", -32_601, cause, data)
    end
  end

  class InvalidParams < Error
    def initialize(cause = nil, data = nil)
      super("Invalid params", -32_602, cause, data)
    end
  end

  class InternalError < Error
    def initialize(cause = nil, data = nil)
      super("Internal error", -32_603, cause, data)
    end
  end

  class ParseError < Error
    def initialize(cause = nil, data = nil)
      super("Parse error", -32_700, cause, data)
    end
  end
end
