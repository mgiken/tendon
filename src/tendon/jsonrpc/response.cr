require "json"
require "http"

require "./errors"

struct Tendon::JSONRPC::Response(T)
  JSON.mapping(jsonrpc: String, id: Int64?, result: T | Nil, error: Error?)

  getter! context : ::HTTP::Server::Context

  forward_missing_to context.response

  def initialize(@context : ::HTTP::Server::Context, @id = context.jsonrpc_id, @jsonrpc = "2.0")
  end

  def respond(@result)
    context.auth_context.flush(context)

    if id
      self.status_code = 200
    else
      @result = nil
      self.status_code = 204
    end
    self.content_type = "application/json"
    self.headers.add("Cache-Control", "no-store")
    self << to_json
  end

  def respond_with_error(@error)
    @result = nil
    self.status_code = 200
    self.content_type = "application/json"
    self.headers.add("Cache-Control", "no-store")
    self << to_json
  end
end
