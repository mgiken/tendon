require "json"

struct Tendon::JSONRPC::Request(T)
  JSON.mapping(jsonrpc: String, id: Int64?, method: String, params: T)
end
