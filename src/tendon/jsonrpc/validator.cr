require "uuid"

module Tendon::JSONRPC::Validator
  extend self

  def validate(type, value)
    case type
    when :required; "Please fill out this field." unless required?(value)
    when :email   ; "Please enter an email address." unless email?(value)
    when :url     ; "Please enter a URL." unless url?(value)
    when :uuid    ; "Please enter a UUID." unless uuid?(value)
    end
  end

  def required?(v)
    !case v
    when String
      v.blank?
    when NamedTuple, Tuple
      v.empty?
    else
      v.nil?
    end
  end

  # ref: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/email#Validation
  # ameba:disable Layout/LineLength
  RE_EMAIL = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/

  def email?(v)
    v.is_a?(String) && v.blank? || v =~ RE_EMAIL
  end

  RE_URL = /^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/

  def url?(v)
    v.is_a?(String) && v.blank? || v =~ RE_URL
  end

  def uuid?(v)
    v.is_a?(String) && UUID.new(v) rescue false
  end
end
