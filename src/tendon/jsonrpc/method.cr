require "../auth/context"
require "../db"

module Tendon::JSONRPC::Method
  include Tendon::DB

  private METHOD_OF = {} of String => TypeNode
  private RE_METHOD = /"method"\s*:\s*"([^"]+)"/m

  private getter context : Tendon::Auth::Context

  forward_missing_to context

  private def initialize(@context)
  end

  private macro require_auth!(role = nil)
    REQUIRE_AUTH = {{ role.id.symbolize }}
  end

  private macro validate(param, *validators)
    {% VALIDATES_OF[param] = validators %}
  end

  macro included
    {% METHOD_OF[@type.name.gsub(/::/, ".").underscore.stringify] = @type %}

    private VALIDATES_OF = {} of String => ASTNode

    def self.call(context, **args)
      method = new(context)
      method.authenticate!
      method.validate!(args)
      method.call(**args)
    end

    macro method_added(method)
      \{% if method.name == "call" %}
        \{% if method.args.size == 0 %}
          alias PARAMS = Hash(String, String)
        \{% else %}
          alias PARAMS = Hash(String, Union(
            \{% for a in method.args %}
              \{{ a.restriction }},
            \{% end %}
          ))

          alias ARGS = NamedTuple(
            \{% for a in method.args %}
              \{{ a.name }}: \{{ a.restriction }},
            \{% end %}
          )

          \{% args = method.args.reject(&.default_value.is_a?(Nop)) %}
          \{% if args.size > 0 %}
            DEFAULTS = {
              \{% for a in args %}
                \{{ a.name.stringify }} => \{{ a.default_value }},
              \{% end %}
            }
          \{% end %}
        \{% end %}

        \{% if !method.return_type.is_a?(Nop) %}
          alias RESULT = \{{ method.return_type }}
        \{% else %}
          alias RESULT = JSON::Any | JSON::Any::Type
        \{% end %}

        def authenticate!
          \{% if @type.has_constant?(:REQUIRE_AUTH) %}
            raise Tendon::HTTP::Unauthorized.new unless context.authenticated?(REQUIRE_AUTH)
          \{% end %}
        end

        def validate!(args)
          return if args.empty?

          errors = Hash(String, Array(String)).new { |h, k| h[k] = [] of String }

          \{% for k, v in VALIDATES_OF %}
            \{% for validate in v %}
              if err = Tendon::JSONRPC::Validator.validate(\{{ validate }}, args[\{{ k.symbolize }}])
                errors[\{{ k.stringify }}] << err
              end
            \{% end %}
          \{% end %}

          raise Tendon::JSONRPC::InvalidParams.new(nil, errors) unless errors.empty?
        end
      \{% end %}
    end

    macro finished
      module Tendon::JSONRPC::Method
        def self.dispatch(context : ::HTTP::Server::Context)
          raise InvalidRequest.new unless context.request.body

          body = context.request.body.not_nil!.gets_to_end
          context.jsonrpc_method = (body.match(RE_METHOD) || raise InvalidRequest.new)[1]

          case context.jsonrpc_method
            \{% for k, v in METHOD_OF %}
              when \{{ k }}
                begin
                  req = Request(::\{{ v }}::PARAMS).from_json(body)
                rescue e
                  raise ParseError.new(e)
                else
                  raise InvalidRequest.new unless req.jsonrpc == "2.0"
                end

                context.jsonrpc_id = req.id

                \{% if v.has_constant?("ARGS") %}
                  \{% if v.has_constant?("DEFAULTS") %}
                    params = ::\{{ v }}::DEFAULTS.deep_merge(req.params)
                  \{% else %}
                    params = req.params
                  \{% end %}

                  begin
                    args = ::\{{ v }}::ARGS.from(params)
                  rescue e
                    raise InvalidParams.new(e)
                  end

                  result = ::\{{ v }}.call(context.auth_context, **args)
                \{% else %}
                  raise InvalidParams.new unless req.params.empty?
                  result = ::\{{ v }}.call(context.auth_context)
                \{% end %}

                Response(::\{{ v }}::RESULT).new(context).respond(result)
            \{% end %}
          else
            raise MethodNotFound.new
          end
        end
      end
    end
  end
end
