require "http"

class Tendon::Handler::ProtectHandler
  include ::HTTP::Handler

  def initialize(@enable_ssl : Bool)
  end

  def call(context)
    context.response.headers.add("Content-Security-Policy", "default-src 'none'; img-src 'self'; script-src 'self'; style-src 'self'")
    context.response.headers.add("Strict-Transport-Security", "max-age=31536000") if @enable_ssl
    context.response.headers.add("X-Content-Type-Options", "nosniff")
    context.response.headers.add("X-Frame-Options", "sameorigin")
    context.response.headers.add("X-XSS-Protection", "1; mode=block")

    call_next(context)
  end
end
