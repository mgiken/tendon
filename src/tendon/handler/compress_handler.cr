require "http"

class Tendon::Handler::CompressHandler < ::HTTP::CompressHandler
  def initialize(@available_types : Array(String))
  end

  def call(context)
    ext = File.extname(context.request.path)

    if ext.empty? || @available_types.includes?(ext)
      super
    else
      call_next(context)
    end
  end
end
