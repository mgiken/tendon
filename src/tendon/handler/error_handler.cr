require "http"

class Tendon::Handler::ErrorHandler
  include ::HTTP::Handler

  def call(context)
    call_next(context)
  rescue e : Tendon::JSONRPC::Error
    Tendon::JSONRPC::Response(Nil).new(context).respond_with_error(e)
    raise e
  rescue e : Tendon::HTTP::Error
    context.response.respond_with_status(e.status)
    raise e
  rescue e : Exception
    context.response.respond_with_status(500)
    raise e
  end
end
