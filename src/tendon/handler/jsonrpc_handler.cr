require "http"

class Tendon::Handler::JSONRPCHandler
  include ::HTTP::Handler

  def call(context)
    return call_next(context) unless context.jsonrpc?

    raise HTTP::MethodNotAllowed.new unless context.request.method == "POST"
    raise HTTP::UnsupportedMediaType.new unless context.request.headers["content-type"]?.try(&.downcase) == "application/json"

    begin
      JSONRPC::Method.dispatch(context)
    rescue e : JSONRPC::Error | HTTP::Error
      raise e
    rescue e
      raise JSONRPC::InternalError.new(e)
    end
  end
end
