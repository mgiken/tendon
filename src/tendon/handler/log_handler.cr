require "http"

class Tendon::Handler::LogHandler
  include ::HTTP::Handler

  def call(context)
    severity = Logger::INFO
    error = nil

    elapsed = Time.measure do
      call_next(context)
    rescue error : Tendon::JSONRPC::Error
      severity = error.code == -32_603 ? Logger::ERROR : Logger::WARN
    rescue error
      # do nothinng
    end

    severity = case context.response.status_code
               when .>= 500; Logger::ERROR
               when .>= 400; Logger::WARN
               else          severity
               end

    log(severity, error, elapsed, context)
  end

  private def log(severity, error, elapsed, context)
    message = "%s %s %s [%d]" % [
      context.request.method,
      context.request.resource,
      context.request.version,
      context.response.status_code,
    ]

    Tendon.logger.log(severity, message, error,
      host: context.request.host || "",
      method: context.request.method,
      path: context.request.resource,
      proto: context.request.version,
      jsonrpc: context.jsonrpc_method || "",
      ip: context.request.remote_ip || "",
      ua: context.request.headers["user-agent"]? || "",
      ref: context.request.headers["referer"]? || "",
      status: context.response.status_code,
      elapsed: "#{elapsed.total_milliseconds.round(2)}ms",
    )
  end
end
