require "http"

class Tendon::Handler::StaticFileHandler < HTTP::StaticFileHandler
  def call(context)
    orig_path = context.request.path

    if context.request.path.ends_with?("/")
      path = File.join(context.request.path, "index.html")
      context.request.path = path if File.exists?(File.join(@public_dir, path))
    end

    super
  ensure
    context.request.path = orig_path if orig_path
  end
end
