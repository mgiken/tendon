require "http"

class Tendon::Auth::Context
  property access_token : String?
  property refresh_token : String?

  def initialize(request : ::HTTP::Request)
    @access_token = request.access_token
    @refresh_token = request.refresh_token
  end

  def authenticated?(role = nil)
    true
  end

  def flush(context)
  end
end
