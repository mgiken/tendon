class HTTP::Request
  TRUSTED_PROXIES = Regex.union(
    /^127\.0\.0\.1$/,                 # localhost IPv4
    /^::1$/,                          # localhost IPv6
    /^10\./,                          # private IPv4 range 10.x.x.x
    /^172\.(1[6-9]|2[0-9]|3[0-1])\./, # private IPv4 range 172.16.0.0 .. 172.31.255.255
    /^192\.168\./,                    # private IPv4 range 192.168.x.x
    /^fc00:/,                         # private IPv6 range fc00
  )

  def remote_ip
    headers["x-forwarded-for"]?.try(&.split(',').reject(&.strip.match(TRUSTED_PROXIES)).last) || remote_address.try(&.split(':').first)
  end

  def xhr?
    headers["x-requested-with"]?.try(&.downcase) == "xmlhttprequest"
  end

  def access_token
    headers["authorization"]?.try(&.split.last)
  end

  def refresh_token
    cookies["refresh_token"]?.try(&.value)
  end
end
