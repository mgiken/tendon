require "http"

class HTTP::Server::Context
  property jsonrpc_method : String?
  property jsonrpc_id : Int64?

  def jsonrpc?
    request.path == Tendon.config.jsonrpc_endpoint
  end

  def auth_context
    @auth_context ||= Tendon::Auth::Context.new(request)
  end
end
