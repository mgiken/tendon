class Hash(K, V)
  def deep_merge(other : Hash(L, W)) forall L, W
    hash = Hash(K | L, V | W).new
    hash.merge!(self)
    hash.deep_merge!(other)
    hash
  end

  def deep_merge!(other : Hash)
    self.merge!(other) do |_, v1, v2|
      v1.is_a?(Hash) && v2.is_a?(Hash) ? v1.deep_merge(v2) : v2
    end
  end
end
